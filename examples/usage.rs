#![no_std]
#![no_main]

use esp32c3_hal::{
    clock::ClockControl, pac::Peripherals, prelude::*, timer::TimerGroup, Delay, Rtc, IO,
};
use esp_backtrace as _;
use esp_println::*;
use riscv_rt::entry;

#[entry]
fn main() -> ! {
    let peripherals = Peripherals::take().unwrap();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();

    // Disable the RTC and TIMG watchdog timers
    let mut rtc = Rtc::new(peripherals.RTC_CNTL);
    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    let mut wdt0 = timer_group0.wdt;
    let timer_group1 = TimerGroup::new(peripherals.TIMG1, &clocks);
    let mut wdt1 = timer_group1.wdt;

    rtc.swd.disable();
    rtc.rwdt.disable();
    wdt0.disable();
    wdt1.disable();

    // Initialise delay timer
    let mut delay = Delay::new(&clocks);

    // Configure dependencies
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let mut trigger = io.pins.gpio6.into_push_pull_output();
    let echo = io.pins.gpio7.into_pull_down_input();

    // Initialize sensor
    let mut hc_sr04 = hc_sr04::HcSr04::new(&mut trigger, &echo);

    loop {
        // Collect distance measurement (in cm)
        // or display error if any but continue
        let distance = match hc_sr04.measure() {
            Ok(d) => d,
            Err(e) => {
                println!("{}", e);
                0.0
            }
        };
        // In practice, you would probably use:
        // let distance = hc_sr04.measure().unwrap_or(0.0)

        println!("{} cm", distance);
        delay.delay_ms(500_u16);
    }
}
