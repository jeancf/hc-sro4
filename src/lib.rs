#![doc = include_str!("../README.md")]

#![allow(dead_code)]
#![no_std]

use core::fmt::Debug;
use embedded_hal::digital::v2::{InputPin, OutputPin};
use esp32c3_hal::systimer::SystemTimer;
use fugit::Duration;

/// System timer runs with a fixed clock frequency of 16 MHz
const SYS_TIM_FREQ: u32 = 16_000_000;

pub struct HcSr04<'l, OP, IP> {
    trigger: &'l mut OP,
    echo: &'l IP,
}

impl<'l, OP: OutputPin, IP: InputPin> HcSr04<'l, OP, IP>
where
    IP::Error: Debug,
    OP::Error: Debug,
{
    /// Create a new instance of the ultrasonic module.
    /// * The generic `OP` parameter accepts any GPIO configured as output.
    /// * The generic `IP` parameter accepts any GPIO configured as input. The input GPIO should also be configured with the weak pull-down resistor (using `into_pull_down_input()` for example).
    pub fn new(trigger: &'l mut OP, echo: &'l IP) -> HcSr04<'l, OP, IP> {
        Self { trigger, echo }
    }

    /// Returns either a distance in centimeters or an error.
    pub fn measure(&mut self) -> Result<f32, HcSr04Error> {
        self.trigger();

        let start = SystemTimer::now();
        let timeout = Duration::<u64, 1, SYS_TIM_FREQ>::millis(20);
        // Wait for echo to go high
        while !self.echo.is_high().unwrap() {
            if SystemTimer::now() - start > timeout.ticks() {
                return Err(HcSr04Error::NoEcho);
            }
        }
        // Start clock
        let start = SystemTimer::now();
        let timeout = Duration::<u64, 1, SYS_TIM_FREQ>::millis(100);
        // Wait for echo to go low
        while self.echo.is_high().unwrap() {
            if SystemTimer::now() - start > timeout.ticks() {
                return Err(HcSr04Error::TooLongEcho);
            }
        }
        // Get clock value
        let end = SystemTimer::now();
        // Calulate and return distance
        Ok(
            (Duration::<u64, 1, SYS_TIM_FREQ>::from_ticks(end - start).to_micros() as f32 / 58.0)
                as f32,
        )
    }

    /// Sends audio signal by holding the trigger pin high during 10 microseconds.
    fn trigger(&mut self) {
        self.trigger.set_high().unwrap();
        // Wait 10 microseconds @ 16 MHz (fixed system clock rate) = 160 ticks
        let duration = Duration::<u64, 1, SYS_TIM_FREQ>::micros(10);
        let start = SystemTimer::now();
        // Block until time has elapsed
        while SystemTimer::now() - start < duration.ticks() {}
        self.trigger.set_high().unwrap();
    }
}

/// Error variants used by this crate.
#[derive(Debug)]
pub enum HcSr04Error {
    /// Timed out waiting for echo.
    NoEcho,
    /// `echo` pin stayed high for too long.
    TooLongEcho,
}

/// Make it possible for `println!()` to be used to display an error message.
impl core::fmt::Display for HcSr04Error {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::NoEcho => write!(f, "Timed out waiting for echo"),
            Self::TooLongEcho => write!(f, "Echo stayed high too long"),
        }
    }
}
