# Driver for Invensense HC-SR04 Ultrasonic Sensor

Provides non-contact ultrasonic measurement function
within a range of 2-400 centimeters with up to 3mm accuracy
(according to the [datasheet](https://www.mouser.com/datasheet/2/813/HCSR04-1022824.pdf))

* `Trigger` pin should be configured as push-pull output GPIO
* `Echo` pin should be configured as input GPIO with weak pull-down resistor

Once the GPIOs are configured, they are used to initialize the driver:

```rust
let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
let mut trigger = io.pins.gpio6.into_push_pull_output();
let echo = io.pins.gpio7.into_pull_down_input();

let mut hc_sr04 = hc_sr04::HcSr04::new(&mut trigger, &echo);
```

to make a distance measurement, use:

```rust
let distance = hc_sr04.measure().unwrap_or(0.0)
```

See also example `usage.rs`.

> The crate is currently not fully based on `embedded-hal` because the implementation of
> clocks and timers in `embedded-hal` is still undefined (as per 1.0.0-alpha8).
> I will make it hardware independent when possible.
> The non hardware-independent part (system clock access) is using `esp32c3-hal`.
